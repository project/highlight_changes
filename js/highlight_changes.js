/**
 * @file
 * User permission page behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Build dialog changes dialog box.
   *
   * @param title
   *   Dialog title.
   * @param body
   *   Body content for the dialog.
   * @param revertCallback
   *   Callback function to handle reverting element.
   * @param properties
   *   Optional properties array for the dialog.
   */
  function changesDialog(title, body, revertCallback, properties) {
    if (properties === undefined) {
      properties = {};
    } else {
      properties = $.extend(true, {}, properties);
    }

    // Dialog title.
    properties.title = title;

    // Create buttons array if it does not exist and add default buttons.
    if (!properties.buttons) {
      properties.buttons = [];
    }
    properties.buttons.push({
      text: "Revert",
      click: function () {
        revertCallback();
        var $text = $("<div> Changes Reverted </div>");
        $("body").append($text);
        var d = $(this).dialog("instance");
        $(this).html("Changes Reverted.");
        d.option("buttons", {});
        $(this).parent().delay(500).fadeOut(500, function() {
          d.close();
          d.destroy();
        });
      }
    });
    properties.buttons.push({
      text: "Close",
      click: function () {
        $(this).dialog("close");
      }
    });

    // Create div to use for dialog, construct content, and create dialog.
    $("body").append($("<div>").attr("id", "hc-dialog"));
    var content = $("#hc-dialog");
    content.html(body);
    return Drupal.dialog(content, properties);
  }

  /**
   * Get items from source whos keys are in removeable.
   *
   * @param source
   *   Object with Key Value items to be diffed.
   * @param removeable
   *   Object with Key Value items to remove from source.
   */
  function diffByKeys(source, removeable) {
    return $(Object.keys(source)).not(Object.keys(removeable)).get().map(function (value, index) {
      return source[value];
    });
  }

  /**
   * Build List of changes. Wrap added item in "hc-added" and removed items in "hc-removed".
   *
   * @param added
   *   Items to render as added.
   * @param removed
   *   Items to render as removed.
   */
  function buildChangesList(added, removed) {
    var $html = $("<div>");
    $html.append($("<h2>").text("Changes"));
    $.each(added, function (index, value) {
      $html
        .append($("<span>").addClass("hc-added").text("+ " + value))
        .append($("<br>"));
    });
    $.each(removed, function (index, value) {
      $html
        .append($("<span>").addClass("hc-removed").text("- " + value))
        .append($("<br>"));
    });
    return $html;
  }


  /**
   * Build and place link for changes dialog and place it at the end of the container.
   *
   * @param $container
   *   Parent component to place the changes link into.
   * @param title
   *   Dialog title.
   * @param body
   *   Body content for the dialog.
   * @param revertCallback
   *   Callback function to handle reverting element.
   * @param properties
   *   Optional properties array for the dialog.
   */
  function buildChangesLink($container, title, body, revertCallback, properties) {
    if (properties === undefined) {
      properties = {};
    }
    var $link = $container.find(".js-hc-changes");
    if ($link.length === 0) {
      $container
        .append(
          $("<div>")
            .addClass("hc-changes js-hc-changes")
            .append(
              $("<a>").text("Changes")
            )
        );
      $link = $container.find(".js-hc-changes");
    }
    $link.click(function () {
      var d = changesDialog(title, body, revertCallback, properties);
      d.showModal();
      return false;
    });
  }

  /**
   * Remove a changes link from a container, if present.
   *
   * @param $container
   *   Parent component to remove the changes link from.
   */
  function removeChangesLink($container) {
    $container.find(".js-hc-changes").remove();
  }

  /**
   * Helper function to construct changes link for multi-select lists and check box options lists.
   *
   * @param $container
   *   Parent component to place the changes link into.
   * @param title
   *   Dialog title.
   * @param in_mapping
   *   Initial key value list of changes. Will be diffed against cur_mapping to find removed values.
   * @param cur_mapping
   *   Current key value list of changes. Will be diffed against cur_mapping to find removed values.
   * @param revertCallback
   *   Callback function to handle reverting element.
   * @param properties
   *   Optional properties array for the dialog.
   */
  function buildSelectableChangesLink($container, title, in_mapping, cur_mapping, revertCallback, properties) {
    if (properties === undefined) {
      properties = {};
    }
    var added = diffByKeys(cur_mapping, in_mapping);
    var removed = diffByKeys(in_mapping, cur_mapping);

    if (added.length > 0 || removed.length > 0) {
      buildChangesLink($container, title, buildChangesList(added, removed), revertCallback, properties);
    } else {
      removeChangesLink($container);
    }
  }

  /**
   * Helper function to construct changes link for single select boxes and radio buttons.
   *
   * @param $container
   *   Parent component to place the changes link into.
   * @param title
   *   Dialog title.
   * @param initial
   *   Initial value of select or radio.
   * @param current
   *   Current value of select or radio.
   * @param revertCallback
   *   Callback function to handle reverting element.
   * @param properties
   *   Optional properties array for the dialog.
   */
  function buildSingleOptionChangesLink($container, title, initial, current, revertCallback, properties) {
    if (properties === undefined) {
      properties = {};
    }
    var initial_keys = Object.keys(initial);
    var initial_val = initial_keys.length > 0 ? initial_keys[0] : false;
    var current_keys = Object.keys(current);
    var current_val = current_keys.length > 0 ? current_keys[0] : false;

    // Construct body, or remove link.
    var body = "";
    if (initial_val && current_val && initial_val !== current_val) {
      body = "Changed from <span class='hc-removed'>" + initial[initial_val] + "</span> to <span class='hc-added'>" + current[current_val] + "</span>.";
    } else if (!initial_val && current_val) {
      body = "Set to <span class='hc-added'>" + current[current_val] + "</span>.";
    } else if (!current_val && initial_val) {
      body = "<span class='hc-removed'>" + initial[initial_val] + "</span> was removed.";
    } else {
      // Remove the link since the value is the same, or not set.
      removeChangesLink($container);
      // Return so we don't build the link.
      return;
    }

    // Build and place link.
    buildChangesLink($container, title, body, revertCallback, properties);
  }

  /**
   * Helper function to construct changes link for text fields.
   *
   * @param $container
   *   Parent component to place the changes link into.
   * @param title
   *   Dialog title.
   * @param initial
   *   Initial value of field for diff.
   * @param current
   *   Current value of field for diff.
   * @param revertCallback
   *   Callback function to handle reverting element.
   * @param properties
   *   Optional properties array for the dialog.
   */
  function buildTextChangesLink($container, title, initial, current, revertCallback, properties) {
    if (properties === undefined) {
      properties = {};
    }
    if (JsDiff === undefined) {
      console.log("The JsDiff library is not available. Highlight Changes is unable to determine changes for text inputs and textareas.")
    }
    // use JsDiff to get the inline diff on the contents.
    var html_changes = (JsDiff !== undefined) ? JsDiff.diffWordsWithSpace(initial, current) : {};

    // JsDiff should always return something, if it does not exist length is 0.
    // If it runs it will return an array with 1 or more elements. For no
    // changes, the length is 1, and the only element will have neither
    if ((html_changes.length === 0) || (html_changes.length === 1 && !html_changes[0].added && !html_changes[0].removed)) {
      // Remove the link since the value is the same, or not set.
      removeChangesLink($container);
      // Return so we don't build the link.
      return;
    }
    // Otherwise, display setup and display the link.
    else {
      var $dialogBody = $("<div>");
      // Determine the header text based on number of changes and type.
      if (html_changes.length === 1) {
        var value = html_changes[0].added ? "Added" : "Removed";
        $dialogBody.append($("<h2>").text(value));
      } else {
        $dialogBody.append($("<h2>").text("Changes"));
      }
      var $html_span = $("<span>").attr("id", "hc-diff-html").addClass("diff");
      html_changes.forEach(function (part) {
        // green for additions, red for deletions
        // grey for common parts
        if (part.added || part.removed) {
          $html_span.append($(((part.added)? "<ins>" : "<del>")).text(part.value));
        } else {
          $html_span.append($("<span>").addClass("no-change").text(part.value));
        }
      });
      $dialogBody.append($html_span);

      // Build and place link.
      buildChangesLink($container, title, $dialogBody, revertCallback, properties);
    }
  }

  /**
   * Helper function to construct changes link for textarea fields.
   *
   * @param $container
   *   Parent component to place the changes link into.
   * @param title
   *   Dialog title.
   * @param initial
   *   Initial value of field for diff.
   * @param current
   *   Current value of field for diff.
   * @param revertCallback
   *   Callback function to handle reverting element.
   * @param properties
   *   Optional properties array for the dialog.
   */
  function buildTextareaChangesLink($container, title, initial, current, revertCallback, properties) {
    if (properties === undefined) {
      properties = {};
    }
    if (JsDiff === undefined) {
      console.log("The JsDiff library is not available. Highlight Changes is unable to determine changes for text inputs and textareas.")
    }

    /*
     * Computing the diff can be a costly operation; especially on long textarea
     * content. The synchronous execution of the change event causes the page to
     * become frozen and unusable while the diff is computed. Thus, we should
     * only compute the diff of the content during the display of the dialog in
     * order to make the ux more pleasant.
     *
     * To do this, we will check the existence of changes by comparing the raw
     * values of the field. Then handle the JsDiff code as part of the dialog's
     * open event.
     */
    if (initial === current) {
      // Remove the link since the value is the same, or not set.
      removeChangesLink($container);
      // Return so we don't build the link.
      return;
    }
    // If strings don't match, setup the dialog and link.
    else {
      // Setup elements for the dialog.
      var $dialogBody = $("<div>");
      var $header = $("<h2>").text("Changes");
      $dialogBody.append($header);
      var $html_span = $("<span>").attr("id", "hc-diff-html").addClass("diff");
      var $text_span = $("<span>").attr("id", "hc-diff-text").addClass("diff");
      $dialogBody.append($html_span);
      $dialogBody.append($text_span);
      $html_span.hide();

      // Add a button to toggle what changes are displayed.
      if (!properties.buttons) {
        properties.buttons = [];
      }
      properties.buttons.push({
        text: "View HTML Changes",
        id: "hc-diff-toggle",
        click: function (event) {
          $html_span.toggle();
          $text_span.toggle();
          var label = ($html_span.is(":visible")) ? "View Text Changes" : "View HTML Changes";
          $(event.target).button("instance").option("label", label);
        }
      });

      // Build and place link.
      buildChangesLink($container, title, $dialogBody, revertCallback, properties);

      /*
       * For textareas, the diff is not rendered until the link it clicked to
       * prevent slow page loads. In order to do this, we need to handle an
       * additional click event to calculate the diffs and render the values in
       * the dialog. We run this after buildChangesLink so its executed before
       * the dialog is displayed.
       */
      var $link = $container.find(".js-hc-changes");
      $link.click(function () {

        var html_changes = (JsDiff !== undefined) ? JsDiff.diffWordsWithSpace(initial, current) : {};
        // Set header text if there is only one HTML change.
        if (html_changes.length === 1) {
          var value = html_changes[0].added ? "Added" : "Removed";
          $header.text(value);
        }
        html_changes.forEach(function (part) {
          // green for additions, red for deletions
          // grey for common parts
          if (part.added || part.removed) {
            $html_span.append($(((part.added)? "<ins>" : "<del>")).text(part.value));
          } else {
            $html_span.append($("<span>").addClass("no-change").text(part.value));
          }
        });

        var text_changes = (JsDiff !== undefined) ? JsDiff.diffWordsWithSpace($(initial).text(), $(current).text()) : {};
        text_changes.forEach(function (part) {
          /*
           * Add ins for additions, del for additions, and wrap non changed in a
           * span with a class so css can be applied to it separately.
           */
          if (part.added || part.removed) {
            $text_span.append($(((part.added)? "<ins>" : "<del>")).text(part.value));
          } else {
            $text_span.append($("<span>").addClass("no-change").text(part.value));
          }
        });

        // If there are no significant text changes, display the HTML ones.
        if ((text_changes.length === 0) || (text_changes.length === 1 && !text_changes[0].added && !text_changes[0].removed)) {
          $(this).parent().find("#hc-diff-toggle").click();
        }
      });
    }
  }

  /**
   * Build key value mapping for options field.
   *
   * @param $list
   *   jQuery Object containing options field to build mapping from.
   */
  function optionsKeyValueMapping($list) {
    var mapping = {};
    $list.each(function () {
      mapping[$(this).val()] = $(this).text();
    });
    return mapping;
  }

  /**
   * Build key value mapping for checkbox.
   *
   * @param $list
   *   jQuery Object containing checkboxes field to build mapping from.
   */
  function checkboxKeyValueMapping($list) {
    var mapping = {};
    $list.each(function () {
      mapping[$(this).val()] = $(this).siblings("label").text();
    });
    return mapping;
  }

  /**
   * Setups script to actively display to users what values were changed.
   *
   * This uses a combination of js and css to change the color or labels and
   * add messages to notify users of what values where changed by the module.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches functionality to entity form.
   */
  Drupal.behaviors.highlight_changes = {
    attach: function (context) {
      $('form').once('highlight_changes').each(function () {
        var $form = $(this);

        // Initialize checkbox and radio inputs.
        $form.find("input[type=checkbox]:checked, input[type=radio]:checked").addClass("hc-initial js-hc-initial");

        // Setup checkboxes.
        $form.find("input[type=checkbox]").change(function () {
          var $changedCheckbox = $(this);
          var $fieldset = $changedCheckbox.closest("fieldset");
          var $initial_options = $fieldset.find(".hc-initial");
          var $current_options = $fieldset.find(":checked");
          var title = $fieldset.find("legend").text();

          // Map convert elements to key values pairs.
          var initial = checkboxKeyValueMapping($initial_options);
          var current = checkboxKeyValueMapping($current_options);

          // Build and place link.
          buildSelectableChangesLink(
            $fieldset,
            title,
            initial,
            current,
            function () {
              $initial_options.not($current_options).attr('checked', 'checked');
              $current_options.not($initial_options).attr('checked', false);
            });
        });

        // setup radio buttons since they only have one value.
        $form.find("input[type=radio]").change(function() {
          var $changedCheckbox = $(this);
          var $fieldset = $changedCheckbox.closest("fieldset");
          var $initial_options = $fieldset.find(".hc-initial");
          var $current_options = $fieldset.find(":checked");
          var title = $fieldset.find("legend").text();

          // Map convert elements to key values pairs.
          var initial = checkboxKeyValueMapping($initial_options);
          var current = checkboxKeyValueMapping($current_options);

          // Build and place link.
          buildSingleOptionChangesLink(
            $fieldset,
            title,
            initial,
            current,
            function () {
              $initial_options.not($current_options).attr('checked', 'checked');
              $current_options.not($initial_options).attr('checked', false);
            });
        });

        // Setup multiple select boxes.
        $form.find("select[multiple]")
          .each(function() {
            var $select = $(this);
            $select.find(":selected").addClass("hc-initial js-hc-initial");
          })
          .change(function() {
            var $select = $(this);
            var $container = $select.parent();
            var title = $select.siblings("label").text();
            var $initial_options = $select.find(".js-hc-initial");

            // Map convert elements to key values pairs.
            var initial = optionsKeyValueMapping($initial_options);
            var current = optionsKeyValueMapping($select.find(":selected"));

            // Build and place link.
            buildSelectableChangesLink(
              $container,
              title,
              initial,
              current,
              function () {
                $select.val(Object.keys($initial_options)).change();
                if ($select.data("chosen")) {
                  $select.trigger('chosen:updated');
                }
              });
          });

        // Setup single select boxes.
        $form.find("select:not([multiple])")
          .each(function() {
            var $select = $(this);
            $select.find(":selected").addClass("hc-initial js-hc-initial");
          })
          .change(function() {
            var $select = $(this);
            var $container = $select.parent();
            var title = $select.siblings("label").text();
            var $initial_options = $select.find(".js-hc-initial");

            // Map convert elements to key values pairs.
            var initial = optionsKeyValueMapping($initial_options);
            var current = optionsKeyValueMapping($select.find(":selected"));

            // Build and place link.
            buildSingleOptionChangesLink($container, title, initial, current, function () {
              $select.val($initial_options.val()).change();
              if ($select.data("chosen")) {
                $select.trigger('chosen:updated');
              }
            });
          });

        // Setup text inputs to process changes with diff.js.
        $form.find("input[type=text], input[type=date], input[type=time]")
          .each(function () {
            var $this = $(this);
            $this.data("_hc_initial", $this.val());
          })
          .on("keyup change", function () {
            var $this = $(this);
            var initial = "";
            if ($this.data("_hc_initial") !== undefined) {
              initial = $this.data("_hc_initial");
            }
            var current = $this.val();
            var title = $this.siblings("label").text();
            var $container = $this.parent();

            buildTextChangesLink($container, title, initial, current, function () {
              $this.val(initial).keyup();
            });
          });

        // Setup passwords. inputs the same as text, but hide data.
        $form.find("input[type=password]")
          .each(function () {
            var $this = $(this);
            $this.data("_hc_initial", $this.val());
          })
          .on("keyup change", function () {
            var $this = $(this);
            var initial = "";
            if ($this.data("_hc_initial") !== undefined) {
              initial = $this.data("_hc_initial");
            }
            initial = initial.replace(/./g, '*');
            var current = $this.val().replace(/./g, '*');
            var title = $this.siblings("label").text();
            var $container = $this.parent();

            buildTextChangesLink($container, title, initial, current, function () {
              $this.val(initial).keyup();
            });
          });

        // Setup textareas to process changes with diff.js.
        $form.find("textarea")
          .each(function () {
            var $this = $(this);
            $this.data("_hc_initial", $this.val());
          })
          .keyup(function () {
            var $this = $(this);
            var initial = "";
            if ($this.data("_hc_initial") !== undefined) {
              initial = $this.data("_hc_initial");
            }
            var current = $this.val();
            var $container = $this.parent();
            var title = $container.siblings("label").text();

            buildTextareaChangesLink($container, title, initial, current, function () {
              $this.val(initial).keyup();
            }, {width: $this.width()});
          });

          // Text areas can be controlled by CKEditor. This will get loaded after the attach.
          // Tell CKEditor to update the textarea and update the link.
          if (CKEDITOR !== undefined) {
            CKEDITOR.on("instanceCreated", function(event) {
              var editor = event.editor;
              var $editor = $(editor);
              $editor.data("_hc_initial", editor.getData());
              editor.on("change", function (changeEvent) {
                var initial = $editor.data("_hc_initial") !== undefined ? $editor.data("_hc_initial") : "";
                var current = editor.getData();
                var $element = $(editor.element.$);
                var $container = $element.parent();
                var title = $container.siblings("label").text();

                buildTextareaChangesLink($container, title, initial, current, function () {
                  editor.setData(initial);
                }, {width: $element.width()});
              });
              editor.on("mode", function () {
                if (this.mode == "source") {
                  var source_textarea = editor.editable();
                  $(source_textarea).change(function (changeEvent) {
                    var $this = $(this);
                    var initial = $editor.data("_hc_initial") !== undefined ? $editor.data("_hc_initial") : "";
                    var current = $this.val();
                    var $element = $(editor.element.$);
                    var $container = $element.parent();
                    var title = $container.siblings("label").text();

                    buildTextareaChangesLink($container, title, initial, current, function () {
                      editor.setData(initial);
                    }, {width: $this.width()});
                  });
                }
              });
            });
          }
      });
    }
  };

})(jQuery, Drupal);
