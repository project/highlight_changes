# Highlight Changes

A Drupal to actively show changes made to forms.

More info can be found at https://www.drupal.org/project/highlight_changes

## CONTENTS OF THIS FILE

 * Introduction
 * Usage
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## INTRODUCTION

The Highlight Changes module provides a way for a user to visually see the
changes made when editing content and entities. The module provides feedback
when values are changed, and an interface for the user to view and revert
changes to a field.

Highlight Changes tracks the initial value of an input when the page is
rendered. When that input's value is changed, a notice is placed next to the
field, that provides a link to view and revert the changes made to the initial
values.

This supports and works with core field widgets, CKEditor, Chosen and other
modules as well.


 * For a full description of the module visit:
   https://www.drupal.org/project/highlight_changes

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/highlight_changes

## USAGE

Highlight Changes will automatically work for any node edit forms. When adding
new nodes, or when editing existing nodes, Highlight Changes will show the
changes that have been made to the form since it loaded. This module has no
knowledge of the original entity values or the entity revisions, and simply
works with using the initial form values on page load.

When a user selects a new item in a list of checkboxes, the label will turn
green. Similarly, when they deselect a checkbox, who started as checked, the
label will turn red.

A "Changes" link appears beneath edited text fields, which displays a visual
diff when clicked.

## REQUIREMENTS

This module requires no modules outside of Drupal Core. This project does use
the Diff JavaScript library to generate visual diffs for text fields. Diff can
be downloaded from https://www.npmjs.com/package/diff. This library must be
installed into Drupal's `libraries` directory.

## INSTALLATION

Install the Highlight Changes module as you would normally install any other
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

After module installation you will need to download the diff module. You can use
the following instrutions to install the diff library package either via Yarn/NPM
or through composer.

### Using Composer

If you are managing your Drupal install via composer, you add the library from
[Asset Packagist](https://asset-packagist.org/). To use Asset Packagist, you can
add the flowing block to the repositories section of your project's
composer.json file.

```
"repositories": [
    {
        "type": "composer",
        "url": "https://asset-packagist.org"
    }
]
```

You will additionally need to map the library to be saved into Drupal's
libraries folder. Add the following to your composer.json.

```
"extra": {
  "installer-paths": {
    "web/libraries/{$name}": ["npm-asset/diff"]
  }
}
```

If you do not wish to use Asset Packagist, you can manually add the following to
use the project directly from npm.

```
  "repositories": [
    {
      "type": "package",
      "package": {
        "name": "npm-asset/diff",
        "version": "3.3.0",
        "type": "drupal-library",
        "dist": {
          "url": "https://registry.npmjs.org/diff/-/diff-3.3.0.tgz",
          "type": "tar"
        },
        "require": {
            "composer/installers": "^1.2.0"
        }
      }
    }
  ]
```

After adding the repository, require the library and module via composer.

```
composer require npm-asset/diff drupal/highlight_changes
```

### Using [npm](https://www.npmjs.com/)

```
mkdir path/to/libraries/diff
npm v diff dist.tarball | xargs curl |
  tar xz --strip-components=1 -C path/to/libraries/diff
```

### Using [Yarn](https://yarnpkg.com/)

```
yarn install --modules-folder path/to/libraries
```

## CONFIGURATION

There are no configuration necessary for Highlight Changes. See 'Usage' for
details on the module.

## MAINTAINERS

* Rich Gerdes (richgerdes) - https://www.drupal.org/u/richgerdes
